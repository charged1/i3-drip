#!/bin/bash

# Intro
echo "Welcome to the i3-drip uninstaller!"

# Stop script if an error occurs anywhere
set -e

# Find distro
source /etc/os-release
# Used to find derivatives' base distro
if [ -v ID_LIKE ]; then
    ID=$ID_LIKE
fi

# Install packages
if [ "$ID" == "arch" ]; then
    # Uninstall uncommon packages
    sudo pacman -Rsn i3-gaps picom lightdm-slick-greeter terminator nitrogen starship i3lock feh python-i3ipc autotiling shell-color-scripts bumblebee-status nerd-fonts-hack
elif [ "$ID" == "debian" ]; then
    # Uninstall uncommon packages
    sudo apt -y autoremove python3-i3ipc terminator fonts-hack nitrogen i3lock feh picom dh-autoreconf libxcb-keysyms1-dev \
    libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev \
    libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 \
    libxcb-xrm-dev libxcb-shape0 libxcb-shape0-dev

    # Uninstall autotiling
    sudo rm -f /usr/local/bin/autotiling

    # Uninstall shell-color-scripts
    sudo rm -rf /opt/shell-color-scripts
    sudo rm -f /usr/local/bin/colorscript

    # Uninstall bumblebee-status
    pip3 uninstall bumblebee-status

    # Uninstall starship
    sudo rm -f /usr/local/bin/starship

    # Uninstall i3-gaps
    rm -f $HOME/.xsession
    sudo rm -rf /usr/local/bin/i3* /usr/local/etc/i3 /usr/local/include/i3 /usr/local/share/applications/i3.desktop /usr/local/share/doc/i3 /usr/local/share/xsessions/i3*
else
    echo "Your distribution is not supported."
    exit 1
fi

# Reset configuration
declare -a files=("picom.conf" "fish/config.fish" "nitrogen/bg-saved.cfg" "nitrogen/nitrogen.cfg" "gtk-3.0/settings.ini" "i3/config" "terminator/config") # set up array of files in ~/.config to make code more compact and efficient
# loop through array and delete all files
for file in "${files[@]}"; do rm -f $HOME/.config/$file; done
# delete special config files manually
rm -f $HOME/.bashrc
sudo rm -f /etc/lightdm/lightdm.conf

# Restore backup configs (if they exist)
pushd $HOME/.config/i3-drip/backup > /dev/null
if [ -e "picom.conf" ]; then mv picom.conf $HOME/.config/picom.conf; fi
if [ -e "config.fish" ]; then mv config.fish $HOME/.config/fish/config.fish; fi
if [ -e "bg-saved.cfg" ]; then mv bg-saved.cfg $HOME/.config/nitrogen/bg-saved.cfg; fi
if [ -e "nitrogen.cfg" ]; then mv nitrogen.cfg $HOME/.config/nitrogen/nitrogen.cfg; fi
if [ -e "settings.ini" ]; then mv settings.ini $HOME/.config/gtk-3.0/settings.ini; fi
if [ -e "terminator-config" ]; then mv terminator-config $HOME/.config/terminator/config; fi
if [ -e "i3-config" ]; then mv i3-config $HOME/.config/i3/config; fi
if [ -e "bashrc" ]; then mv bashrc $HOME/.bashrc; fi
if [ -e "lightdm.conf" ]; then sudo mv lightdm.conf /etc/lightdm/lightdm.conf; fi
popd > /dev/null

# Delete ~/.config/i3-drip
rm -rf $HOME/.config/i3-drip

# Unapply dracula theme
rm -rf $HOME/.themes/dracula
rm -rf $HOME/Pictures/dracula-wallpapers

# End
cat <<EOF
Uninstallation complete!
A reboot is recommended for the new configuration to apply.
EOF
